const dotenv = require('dotenv');
const mongoose = require('mongoose');
const axios = require('axios');
const Cocktail = require('./models/cocktailModel');

// Fetch environmental variables
dotenv.config();

// MongoDB database connection
const connectDB = async () => {
  try {
    const conn = await mongoose.connect(process.env.MONGODB_URI, {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    });
    console.log(`MongoDB connected at ${conn.connection.host}`);
  } catch (error) {
    console.error(`MongoDB connecting Error: ${error.message}`);
    process.exit(1);
  }
};

// Get cocktails from thecocktailsdb
const fetchCocktail = async () => {
  const API_URL = 'https://www.thecocktaildb.com/api/json/v1/1/search.php?f=a';
  try {
    const res = await axios.get(API_URL);

    return res.data.drinks.map((drink) => ({
      strDrink: drink.strDrink,
      strInstructions: drink.strInstructions,
    }));
  } catch (error) {
    console.log('error in fetching cocktails', error);
  }
};

// Save cocktails to MongoDB
const saveCocktails = async () => {
  // Connecting Database
  connectDB();

  // Fetching Cocatails data

  const cocktails = await fetchCocktail();

  // Sorting results alphabetically
  cocktails.sort((a, b) => a.strDrink.localeCompare(b.strDrink));
  // console.log(cocktails);

  // Saving fetched cocktails
  return Promise.all(
    cocktails.map(async ({ strDrink, strInstructions }) => {
      try {
        const cocktail = await Cocktail.findOne({ strDrink });
        cocktail
          ? await Cocktail.findOneAndUpdate(
              { strDrink },
              { strDrink, strInstructions }
            )
          : await Cocktail.create({ strDrink, strInstructions });
      } catch (error) {
        (error) => console.log('cocktail saving error', error);
      }
    })
  );
};

saveCocktails().then(() => {
  console.log('Cocktails saved in Database'), process.exit();
});
