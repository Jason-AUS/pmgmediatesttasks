const mongoose = require('mongoose');

const cocktailSchema = mongoose.Schema({
  strDrink: {
    type: String,
    required: true,
  },
  strInstructions: {
    type: String,
    required: true,
  },
});

module.exports = mongoose.model('Cocktail', cocktailSchema);
