const receiptData = require('./data');

const re = /^\d{1,3}(?:[,]\d{3})*(?:[.]\d{2})$/;

const products = [];

receiptData.map((string, index, receiptData) => {
	if (string.match(re))
		products.push([receiptData[index - 1], receiptData[index]]);
});

console.log(products);

console.log('Total Items:', products.length);

process.exit(0);
